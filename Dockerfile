FROM python:3.12-slim

# Set environment variables for Python
ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

# Set working directory
WORKDIR /app

# Install required system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    git \
    git-lfs \
    openssh-client \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install Python dependencies
COPY deploy/requirements.txt /app/deploy/requirements.txt
RUN python3 -m venv /app/venv \
    && /app/venv/bin/pip install --upgrade pip \
    && /app/venv/bin/pip install --no-cache-dir -r /app/deploy/requirements.txt

# Copy application code
COPY . /app

# Configure Git user (can be overridden via environment variables)
ARG GIT_USER="blender-bot"
ARG GIT_EMAIL="blender-bot"
RUN git config --global user.name "$GIT_USER" \
    && git config --global user.email "$GIT_EMAIL"
RUN mkdir -p ~/.ssh

# Expose the webhook port
EXPOSE 4000

# Define entrypoint for the container
CMD ["/app/venv/bin/gunicorn", "-w", "1", "blender_bot:app", \
    "-b", "0.0.0.0:4000", \
    "--error-logfile", "/app/dist/logs/error.log", \
    "--access-logfile", "/app/dist/logs/access.log", \
    "--capture-output"]
