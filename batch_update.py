#!/usr/bin/python3

import notes
import utils

utils.git_remove_all_notes("blender/blender")
notes.batch_update_commits("blender/blender")
notes.batch_update_issues("blender/blender")
utils.git_push_notes("blender/blender")
