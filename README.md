# Blender Bot for Gitea

## Commands

Blender developers with commit access can start builds by typing commands in comments on projects.blender.org.

* `@blender-bot build [flags] [platforms]`: build current pull request. A notification about the build progress / result is shown in the list of commits that make up this pull request.
* `@blender-bot package [flags] [platforms]`: build current pull request and make package for download. The bot will post the link in a comment on the pull request.
* `@blender-bot build-branch branch [flags] [platforms]`: build the given branch. The bot will post a link to the build in a comment on the issue / pull request.
* `@blender-bot package-branch branch [flags] [platforms]`:  build branch and make package for download. The bot will post the link in a comment on the issue / pull request.

`[platforms]` is optional, and used to build for one or more platforms. Possible values:
* `windows` `macos` `linux` (for brevity)
* `windows-amd64` `windows-arm64` `macos-x86_64` `macos-arm64` `linux-x86_64` (to be specific)

`[flags]`
* `+bpy`: build Blender as a Python module
* `+san`: use sanitizer build configuration
* `+test_gpu_compositor`: add compositor GPU tests
* `+test_gpu_cycles`: add Cycles GPU tests
* `+test_gpu_draw`: add EEVEE, Workbench and GPU module tests
* `+test_gpu_usd`: add USD and Hydra Storm tests
* `+test_gpu_all`: add all possible GPU related tests

Other:

* `@blender-bot ping`: test if blender-bot is online

## Automation

* When an issue is automatically closed or reopened through a commit, set the
  resolved and confirmed status.
* When an issue is manually closed or reopened, set archived and confirmed or
  needs triage status, unless a valid closed or open status is already set.
* Add git notes to commits to show issues, pull requests and commits that
  reference them.
