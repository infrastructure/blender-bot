from pathlib import Path
import os

def get_secret(env_var_name, file_path):
    """
    Retrieves a secret from an environment variable or a file.
    Priority: 
    1. Environment variable
    2. File content
    Raises an exception if neither is available.
    """
    # Try getting the value from the environment variable
    value = os.environ.get(env_var_name)
    if value:
        return value

    # Fallback to reading the value from the file
    file = Path(file_path)
    if file.is_file():
        return file.read_text().strip()

    # Raise an error if neither method works
    raise RuntimeError(f"Missing required secret: {env_var_name} or file {file_path}")

# Gitea server.
GITEA_HOST = os.environ.get('GITEA_HOST', "10.0.2.226")
GITEA_HTTP_PORT = int(os.environ.get('GITEA_HTTP_PORT', 3000))
GITEA_SSH_PORT = int(os.environ.get('GITEA_SSH_PORT', 10022))

# Port used when running in debug mode.
DEBUG_WEBHOOK_PORT = int(os.environ.get('DEBUG_WEBHOOK_PORT', 4000))

# Folder for location data files.
DIST_DIR = Path(__file__).resolve().parent / "dist"
SECRETS_DIR = DIST_DIR / "secrets"

# Gitea API.
GITEA_API_TOKEN = get_secret('GITEA_API_TOKEN', SECRETS_DIR / "gitea-api-token")
GITEA_BASE_URL = f"http://{GITEA_HOST}:{GITEA_HTTP_PORT}"
GITEA_BASE_PUBLIC_URL = os.environ.get('GITEA_BASE_PUBLIC_URL', "https://projects.blender.org")
GITEA_API_URL = GITEA_BASE_URL + "/api/v1"
GITEA_API_PUBLIC_URL = GITEA_BASE_PUBLIC_URL + "/api/v1"

# Repositories the webhook is used for.
GITEA_REPOS = [
    "blender/blender",
    "blender/blender-addons",
    "blender/blender-manual",
]

# Buildbot.
BUILDBOT_BASE_URL = os.environ.get('BUILDBOT_BASE_URL', "https://builder.blender.org/admin")
BUILDBOT_DOWNLOAD_URL = os.environ.get('BUILDBOT_DOWNLOAD_URL', "https://builder.blender.org/download")
BUILDBOT_USERNAME = os.environ.get('BUILDBOT_USERNAME', "blender-bot")
BUILDBOT_PASSWORD = get_secret('BUILDBOT_PASSWORD', SECRETS_DIR / "buildbot-password")

# Local git repositories for notes.
REPOS_DIR = DIST_DIR / "repos"
REPO_BASE_URL = f"ssh://git@{GITEA_HOST}:{GITEA_SSH_PORT}/"

# For replying to commands.
INFO_MESSAGE = "See [documentation](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md) for details."
