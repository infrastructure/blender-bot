# Blender Bot for Gitea

### Dependencies

On FreeBSD:

    python39 py39-pip git git-lfs

### Python

Setup isolated Python environment.

    mkdir -p dist
    python3 -m venv dist/venv
    ./dist/venv/bin/pip3 install -r deploy/requirements.txt

### Git

Setup Git user and SSH key for committing and pushing notes to Git repos.

    git config --global user.name "blender-bot"
    git config --global user.email "blender-bot"

    ssh-keygen

### Gitea

Configure Gitea app.ini to be able to access the webhook:

    [webhook]
    ALLOWED_HOST_LIST = 127.0.0.1

Create blender-bot account
* Generate application token with `repo`, `repo_status`, `public_repo`,
  `read:org` scopes enabled. Store it in `dist/secrets/gitea-api-token`.
* Set buildbot password for `blender-bot` acount there in
  `dist/secrets/buildbot-password`.
* Set SSH key in account.
* The account should have ability to edit issues and push to the repositories
  listed in config.py.

In the Blender organization, add a Webhook of type Gitea:

    Target URL: http://127.0.0.1:4000/webhook
    HTTP Method: POST
    Trigger On: All Events

Run one time setup to clone git reposities:

    ./dist/venv/bin/python3 setup.py

## Run

For production:

    ./deploy/start.sh
    ./deploy/stop.sh

For debugging:

    ./deploy/debug.sh

## Docker

For Docker setups, a `Dockerfile` has been added for the creation of a Python environment including the code, its dependencies and standard configuration such as for Git.

In the `docker-compose.yml` it we declare how to deploy such a container, including where it gets its' environment variables from and what kind of bind volumes it will need (for logging and data storage)

To get started, you will first need a `.env` file with the appropriate values. Some defaults can be taken over from `.env.example`, which you can copy. 

When all required values have been set, you can start the application with:

```
docker compose up -d --build
```